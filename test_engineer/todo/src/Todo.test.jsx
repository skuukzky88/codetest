//Jest For Selenium JavaScript Testing
/**
 * @jest-environment jest-environment-webdriver
 */

//I used this article https://www.lambdatest.com/blog/jest-tutorial-for-selenium-javascript-testing-with-examples/ to learn more about using the selenium framework
//with javascript


require('chromedriver');
const webdriver = require('selenium-webdriver');
const url = 'http://localhost:3000/'
const driver = new webdriver.Builder()
    .forBrowser('chrome')
    .build();


describe('executing test scenarios for the todo app', () => {

    test('checks if it loads with the right title', () => {
        driver.get(url);
        const title = driver.findElement(webdriver.By.tagName('h1')).getText();
        expect(title).toContain('Todo list');
    })

    test('checks if there is a Todo list',  () => {
        const startInterval = () => {
            const elements = driver.findElements(webdriver.By.className('Todo-group'));
            setInterval(() => {
                elements.isDisplayed();
            }, 500);
        };

        const init = () => {
            driver.get(url);
            driver.wait(webdriver.until.elementLocated(webdriver.By.className('Todo-group')));
            startInterval();
        };

        init();
    })

    test('checks if the add button is working', async () => {
        driver.get(url);
        var el = driver.findElement(webdriver.By.className('submit'));
        el.click();
        expect
    })

    test('checks if it allows to create a todo', () => {
        driver.get(url);
        const task = "Cook dinner";
        driver.findElement(webdriver.By.className('form-control')).sendKeys(task);
        driver.findElement(webdriver.By.className('submit')).click();
        const actual = driver.findElement(webdriver.By.xpath("//*[text()='Cook dinner']")).getText();

        expect(actual).toBe(task);
    });

    test('checks if marking the checkbox done or undone button is working', () => {
        driver.get(url);
        const el = driver.findElement(webdriver.By.className('icon'));
        el.click();
    })

    test('check if after marking the task done it is actually marked as completed', () => {
        driver.get(url);
        driver.findElement(webdriver.By.css('.undone')).click();
        const actualDone = driver.findElement(webdriver.By.css('.done')).getText();
        const doneText = "[X]";
        expect(actualDone).toBe(doneText);
    })

    test('checks if the delete button is working', () => {
        driver.get(url);
        const el = driver.findElement(webdriver.By.className('close'));
        el.click();
    })

    driver.quit();
})

